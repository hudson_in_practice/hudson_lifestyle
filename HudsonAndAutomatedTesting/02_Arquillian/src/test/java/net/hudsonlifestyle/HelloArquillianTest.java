package net.hudsonlifestyle;

import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class HelloArquillianTest {
    
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(HelloArquillian.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }    
    
    @Inject
    HelloArquillian myHello;
    
    @Test
    public void testPopulateArray() throws Exception {
        String [] oneElement = new String[1];
        myHello.setFirstElement(oneElement);
        assertNotNull(oneElement[0]);
        
    }
}
