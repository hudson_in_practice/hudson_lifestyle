package net.hudsonlifestyle;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class HelloArquillian {
    
    public void setFirstElement(String [] preAllocated) {
        preAllocated[0] = "" + System.currentTimeMillis();
    }
    
}
