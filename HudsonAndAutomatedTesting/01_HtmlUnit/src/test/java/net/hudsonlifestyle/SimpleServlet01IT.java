package net.hudsonlifestyle;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
        

public class SimpleServlet01IT {
    
    private String webUrl;

    private WebClient webClient;

    @Before
    public void setUp() {
        webUrl = System.getProperty("integration.base.url") + "/SimpleServlet01";
        webClient = new WebClient();
    }

    @After
    public void tearDown() {
        webClient.closeAllWindows();
    }

    @Test
    public void testIndexHtml() throws Exception {
        System.out.println("Connecting to: " + webUrl);
        HtmlPage page = webClient.getPage(webUrl);
        assertTrue(page.getBody().asText().indexOf("Servlet SimpleServlet01 at") != -1);
    }
    

}
